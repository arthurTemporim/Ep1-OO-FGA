#ifndef PROCESSAOUT_HPP
#define PROCESSAOUT_HPP

#include "imagem.hpp"

using namespace std;

class ProcessaOut : public Imagem {
	private:
		ofstream * criarImagem;

	public:
		ProcessaOut();
		ProcessaOut(ofstream * criarImagem);

		ofstream * getCriarImagem();
		void setCriarImagem(ofstream * criarImagem);

		int Criacao(const char * nome);
};

#endif 