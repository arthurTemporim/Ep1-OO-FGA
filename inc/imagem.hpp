#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

using namespace std;

class Imagem {
	private:
		string numeroMagico, comentario;
		int altura, largura, maximo;
		char *pixels; 

	public:
		Imagem();
		Imagem(string numeroMagico, string comentario, int altura, int largura, int maximo, char *pixels);

		string getNumeroMagico();
		void setNumeroMagico(string numeroMagico);

		string getComentario();
		void setComentario(string comentario);

		int getAltura();
		void setAltura(int altura);

		int getLargura();
		void setLargura(int largura);

		int getMaximo();
		void setMaximo(int maximo);

		char * getPixels();
		void setPixels(char * pixels);
	
};

#endif