#ifndef PROCESSA_HPP
#define PROCESSA_HPP

#include "imagem.hpp"

using namespace std;

class Processa : Imagem {
	private:
		ifstream * abrirImagem;
		ofstream * criarImagem;

	public:
		Processa();
		Processa(ifstream * abrirImagem);
		Processa(ofstream * criarImagem);

		ifstream * getAbrirImagem();
		void setAbrirImagem(ifstream * abrirImagem);

		ofstream * getCriarImagem();
		void setCriarImagem(ofstream * criarImagem);

		int Abertura(const char * nome);
		int Criacao(const char * nome);
};

#endif 