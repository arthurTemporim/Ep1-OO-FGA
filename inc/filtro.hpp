#ifndef FILTRO_HPP
#define FILTRO_HPP

#include <iostream>

using namespace std;

class Filtro {
	private:
		int div;
		int size;

	public:
		Filtro();
		Filtro(int div, int size);

		int getDiv();
		void setDiv(int div);

		int getSize();
		void setSize(int size);
};

#endif