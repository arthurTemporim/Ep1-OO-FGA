#include "imagem.hpp"

Imagem::Imagem(){
	setNumeroMagico("P5");
	setComentario("ConstrutorVazio");
	setAltura(255);
	setLargura(255);
	setMaximo(522);
	setPixels(NULL);
}

Imagem::Imagem(string numeroMagico, string comentario, int altura, int largura, int maximo, char *pixels) {
	setNumeroMagico(numeroMagico);
	setComentario(comentario);
	setAltura(altura);
	setLargura(largura);
	setMaximo(maximo);
	setPixels(pixels);
}

string Imagem::getNumeroMagico() {
	return numeroMagico;
}
void Imagem::setNumeroMagico(string numeroMagico) {
	this -> numeroMagico = numeroMagico;
}

string Imagem::getComentario() {
	return comentario;
}
void Imagem::setComentario(string comentario) {
	this -> comentario = comentario;
}

int Imagem::getAltura() {
	return altura;
}
void Imagem::setAltura(int altura) {
	this -> altura = altura;
}

int Imagem::getLargura() {
	return largura;
}
void Imagem::setLargura( int largura) {
	this -> largura = largura;
}

int Imagem::getMaximo() {
	return maximo;
}
void Imagem::setMaximo(int maximo) {
	this -> maximo = maximo;
}

char * Imagem::getPixels() {
	return pixels;
}
void Imagem::setPixels(char * pixels) {
	this -> pixels = pixels;
}