#include <iostream>

#include "processaIn.hpp"
#include "processaOut.hpp"
#include "negativo.hpp"
#include "smooth.hpp"
#include "sharpen.hpp"

using namespace std;

int main() {
	
	int max;
	const char * nomeImagem;
	const char * nomeSaida;
	string arquivo;
	string arquivoFinal;
	string opcao;
	char *novoPixel;

	
	ifstream imagem;
	ofstream novaImagem;
	
	ProcessaIn processaIn;
	ProcessaOut processaOut;

	Negativo negativo;
	Sharpen sharpen;
	Smooth smooth;


	cout << "Digite o nome e o caminho da imagem." << endl;
	cin >> arquivo;

	nomeImagem = arquivo.c_str();

	processaIn.setAbrirImagem(&imagem);
	processaOut.setCriarImagem(&novaImagem);

	if(processaIn.Abertura(nomeImagem)) {
		cout << "ERRO! Não foi possível abrir o arquivo." << endl;
		return 0;
	}
	cout << "\nA imagem está em: " << arquivo << endl;

	cout << "\nDigite o nome e onde deve ficar a nova imagem." << endl;
	cin >> arquivoFinal;

	
	while(1){
		cout << endl << "Escolha um filtro:\n";
		cout << "Negativo: 1\n" << "Smooth: 2\n" << "Sharpen: 3\n" << "Opção: ";
		cin >> opcao;
		if(opcao == "1" || opcao == "2" || opcao == "3")
			break;
		cout << "ERRO! número incorreto. Tente novamente\n";
	}
		
	max = processaIn.getMaximo();

	if(opcao == "1"){
		novoPixel = negativo.Filtra(processaIn.getPixels(), processaIn.getLargura(), processaIn.getAltura(), &max);
	}
	else if(opcao == "2"){
		novoPixel = smooth.Filtra(processaIn.getPixels(), processaIn.getLargura(), processaIn.getAltura(), &max);
	}
	else{
		novoPixel = sharpen.Filtra(processaIn.getPixels(), processaIn.getLargura(), processaIn.getAltura(), &max);
	}
	
	nomeSaida = arquivoFinal.c_str();

	
	processaOut.setNumeroMagico(processaIn.getNumeroMagico());
	processaOut.setComentario(processaIn.getComentario());
	processaOut.setLargura(processaIn.getLargura());
	processaOut.setAltura(processaIn.getAltura());
	processaOut.setMaximo(max);
	processaOut.setPixels(novoPixel);
	
	if(processaOut.Criacao(nomeSaida)){
		cout << "ERRO!\nFalha a nova imagem!" << endl;
		return 0;
	}
	
	cout << "Nova imagem criada com SUCESSO!" << endl;
	
	return 0;

}