#include "sharpen.hpp"

using namespace std;

Sharpen::Sharpen(){
	// 1 pois não calcula média.
	setDiv(1);
	// Tamanho da mini matriz de comparação 3x3.
	setSize(3);
}

char * Sharpen::Filtra(char *pixels, int largura, int altura, int *maximo){
	int value, i, j;
	// Valor do size inserido no construtor.
	int size = getSize();
	// Matriz de comparação dada para aplicar o filtro.
	int matriz[] = {0, -1, 0, -1, 5, -1, 0, -1, 0};
	// Valor do div inserido no construtor.
	int div = getDiv();
	//Vetor de'pixels com o tamanho da imagem.
	char *novoPixel = new char[largura*altura];
	if(novoPixel == NULL){
		return NULL;
	}

	for(i=0; i<size/2; i++){
		for(j=0; j<largura; j++){
			novoPixel[i+j*largura] = pixels[i+j*largura];
		}
	}
	
	for(i=0; i<size/2; i++){
		for(j=0; j<altura; j++){
			novoPixel[i*altura+j] = pixels[i*altura+j];
		}
	}

	int x = 0;
	int y = 0;
	// Algoritmo dado.
	for(i=size/2; i<largura-size/2; i++){
		for(j=size/2; j<altura-size/2; j++){
			value = 0;
			for(x=-(size/2); x<=size/2; x++){
				for(y=-(size/2); y<=size/2;y++){
					value += matriz[(x+1)+size*(y+1)]*(unsigned char)pixels[(i+x)+(y+j)*largura];
				}
			}
				value /= div;
				
				value = value < 0 ? 0 : value;
				value = value > 255 ? 255 : value;
						
				novoPixel[i+j*largura] = value;
		}
	}
	// Retorna um vetor de pixels com o filtro aplicado.
	return novoPixel;
}
