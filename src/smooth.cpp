#include "smooth.hpp"

using namespace std;

Smooth::Smooth(){
	// 9 pois contém a média dos pontos da matriz.
	setDiv(9);
	// Tamanho da mini matriz de comparação 3x3.
	setSize(3);
}

char * Smooth::Filtra(char *pixels, int largura, int altura, int *maximo){
	int value, i, j; 
	int size = getSize(), matriz[] = {1, 1, 1, 1, 1, 1, 1, 1, 1}, div = getDiv();
	char *novoPixel = new char[largura*altura];
	if(novoPixel == NULL){
		return NULL;
	}

	for(i=0; i<size; i++){
		for(j=0; j<largura; j++){
			novoPixel[i+j*largura] = pixels[i+j*largura];
		}
	}
	
	for(i=0; i<size; i++){
		for(j=0; j<altura; j++){
			novoPixel[i*altura+j] = pixels[i*altura+j];
		}
	}

	int x = 0;
	int y = 0;
	// Percorre a imagem.
	for(i=size/2; i<largura; i++){
		for(j=size/2; j<altura; j++){
			value = 0;
			// size/2 é 1, nesse caso, então vai de -1 até 1.
			// Esses 2 fors percorrem uma matriz 3x3 (-1, 0, 1).
			for(x=-(size/2); x<=size/2; x++){
				for(y=-(size/2); y<=size/2;y++){
					// value contém soma dos valores tratados da matriz 3x3
					value += matriz[(x+1)+size*(y+1)]*(unsigned char)pixels[(i+x)+(y+j)*largura];
				}
			}
				// value vai valer a média já que div = 9.
				value /= div;
				
				value = value < 0 ? 0 : value;
				value = value > 255 ? 255 : value;
				
				// Depois de descobrir o valor do filtro, ele é aplicado ao pixel.
				novoPixel[i+j*largura] = value;
		}
	}
	
	// Retorna um vetor de pixels tratados.
	return novoPixel;
}
