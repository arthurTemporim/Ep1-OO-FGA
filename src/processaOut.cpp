#include "processaOut.hpp"

using namespace std;

ProcessaOut::ProcessaOut() {
	setCriarImagem(NULL);
}
//Criar:
ProcessaOut::ProcessaOut(ofstream * criarImagem) {
	setCriarImagem(criarImagem);
}

ofstream * ProcessaOut::getCriarImagem() {
	return criarImagem;
}
void ProcessaOut::setCriarImagem(ofstream * criarImagem) {
	this -> criarImagem = criarImagem;
}

//Método usado:
int ProcessaOut::Criacao(const char * nome) {
	criarImagem->open(nome);

	if(!(criarImagem->is_open()))
		return -1;

	char *pixels = getPixels();
	int largura = getLargura();
	int altura = getAltura();

	*criarImagem << getNumeroMagico() << endl;
	if(getComentario() != "vazio")
		*criarImagem << getComentario() << endl;
	*criarImagem << largura << ' ' << altura << endl << getMaximo() << endl;
		
	for(int i = 0; i < largura; i++){
		for(int j = 0; j < altura; j++){
				criarImagem->put(pixels[i*largura+j]);
		}
	}
	
	criarImagem->close();
	
	return 0;

}