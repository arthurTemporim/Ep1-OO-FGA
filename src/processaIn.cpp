#include "processaIn.hpp"

using namespace std;

ProcessaIn::ProcessaIn() {
	setAbrirImagem(NULL);
}

//Abrir:
ProcessaIn::ProcessaIn(ifstream * abrirImagem) {
	setAbrirImagem(abrirImagem);
}

ifstream * ProcessaIn::getAbrirImagem() {
	return abrirImagem;
}
void ProcessaIn::setAbrirImagem(ifstream * abrirImagem) {
	this -> abrirImagem = abrirImagem;
}

//Método usado:
int ProcessaIn::Abertura(const char * nome) {

	abrirImagem->open(nome);
	if(!(abrirImagem->is_open()))
		return -1;
	
	int altura, largura, maximo;
	string numeroMagico, comentario;
	stringstream a;
	
	getline(*abrirImagem, numeroMagico);
	if(numeroMagico != "P5")
		return -1;
	
	setNumeroMagico(numeroMagico);
	getline(*abrirImagem, comentario);
	if(comentario[0] == '#'){
		setComentario(comentario);
		*abrirImagem >> largura >> altura;
	}
	else{
		a.str(comentario);
		a >> largura >> altura;
	}
	*abrirImagem >> maximo;
	
	setLargura(largura);
	setAltura(altura);
	setMaximo(maximo);
	
	abrirImagem->get();
	
	char *pixels = new char[largura*altura];
	if(pixels == NULL){
		return -1;
	}
	setPixels(pixels);
	
	for(int i = 0; i < largura; i++){
		for(int j = 0; j < altura; j++){
			abrirImagem->get(pixels[i*largura+j]);
		}
	}
	
	abrirImagem->close();
	
	return 0;
}