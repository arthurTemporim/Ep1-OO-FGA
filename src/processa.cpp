#include "processa.hpp"

using namespace std;

Processa::Processa() {
	setAbrirImagem(NULL);
	setCriarImagem(NULL);
}

//Abrir:
Processa::Processa(ifstream * abrirImagem) {
	setAbrirImagem(abrirImagem);
}

ifstream * Processa::getAbrirImagem() {
	return abrirImagem;
}
void Processa::setAbrirImagem(ifstream * abrirImagem) {
	this -> abrirImagem = abrirImagem;
}

//Criar:
Processa::Processa(ofstream * criarImagem) {
	setCriarImagem(criarImagem);
}

ofstream * Processa::getCriarImagem() {
	return criarImagem;
}
void Processa::setCriarImagem(ofstream * criarImagem) {
	this -> criarImagem = criarImagem;
}

//Método usado:
int Processa::Abertura(const char * nome) {

	abrirImagem->open(nome);
	if(!(abrirImagem->is_open()))
		return -1;
	
	int altura, largura, maximo;
	string numeroMagico, comentario;
	stringstream a;
	
	getline(*abrirImagem, numeroMagico);
	if(numeroMagico != "P5")
		return -1;
	
	setNumeroMagico(numeroMagico);
	getline(*abrirImagem, comentario);
	if(comentario[0] == '#'){
		setComentario(comentario);
		*abrirImagem >> largura >> altura;
	}
	else{
		a.str(comentario);
		a >> largura >> altura;
	}
	*abrirImagem >> maximo;
	
	setLargura(largura);
	setAltura(altura);
	setMaximo(maximo);
	
	abrirImagem->get();
	
	char *pixels = new char[largura*altura];
	if(pixels == NULL){
		return -1;
	}
	setPixels(pixels);
	
	for(int i = 0; i < largura; i++){
		for(int j = 0; j < altura; j++){
			abrirImagem->get(pixels[i*largura+j]);
		}
	}
	
	abrirImagem->close();
	
	return 0;
}

int Processa::Criacao(const char * nome) {
	criarImagem->open(nome);

	if(!(criarImagem->is_open()))
		return -1;

	char *pixels = getPixels();
	int largura = getLargura();
	int altura = getAltura();

	*criarImagem << getNumeroMagico() << endl;
	if(getComentario() != "vazio")
		*criarImagem << getComentario() << endl;
	*criarImagem << largura << ' ' << altura << endl << getMaximo() << endl;
		
	for(int i = 0; i < largura; i++){
		for(int j = 0; j < altura; j++){
				criarImagem->put(pixels[i*largura+j]);
		}
	}
	
	criarImagem->close();
	
	return 0;

}