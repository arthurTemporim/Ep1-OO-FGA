#include "filtro.hpp"

using namespace std;

Filtro::Filtro(){
	setDiv(0);
	setSize(0);
}

Filtro::Filtro(int div, int size) {
	setDiv(div);
	setSize(size);
}

int Filtro::getDiv(){
	return div;
}
void Filtro::setDiv(int div){
	this -> div = div;
}

int Filtro::getSize(){
	return size;
}
void Filtro::setSize(int size){
	this -> size = size;
}
