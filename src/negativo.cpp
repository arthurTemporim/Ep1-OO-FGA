#include "negativo.hpp"

using namespace std;

Negativo::Negativo(){
}

char * Negativo::Filtra(char *pixels, int largura, int altura, int *maximo){
	char *novoPixel = new char[largura*altura];
	if(novoPixel == NULL){
		return NULL;
	}
	
	for(int i=0; i<largura; i++){
		for(int j=0; j<altura; j++){
			novoPixel[i+j*largura] = (char)(*maximo - pixels[i+j*largura]);
		}
	}
	
	return novoPixel;
}
